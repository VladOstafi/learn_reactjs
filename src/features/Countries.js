import React, { useEffect, useState } from 'react';

const API_URL = 'https://restcountries.eu/rest/v2/name/';

const Countries = () => {
  const [searchValue, setSearchValue] = useState('');
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      if (!searchValue) return;

      const res = await fetch(API_URL + searchValue);
      const data = await res.json();

      setData(data);
    };

    fetchData();
  }, [searchValue]);

  const onChange = (event) => {
    const { value } = event.target;

    setSearchValue(value);
  };

  return (
    <div className="countries-container">
      <input type="text" value={searchValue} onChange={onChange} />
      {data.map(country => (
        <span key={country.name}>{country.name}</span>
      ))}
    </div>
  );
};

export default Countries;