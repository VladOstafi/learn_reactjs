import React, { useState } from 'react';

const Counter = () => {
  const [count, setCount] = useState(0);

  return (
    <div className="counter-container">
      <button onClick={() => setCount(count + 1)}>
        +
      </button>
      <div>{count}</div>
      <button onClick={() => setCount(count - 1)}>
        -
      </button>
    </div>
  );
};

export default Counter;