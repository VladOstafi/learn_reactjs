import React from 'react';

const tabs = ['Counter', 'Countries'];

const Header = (props) => {
  return (
    <header className="header">
      {tabs.map(tabName => (
        <span
          key={tabName}
          className={`header-item ${props.tab === tabName ? 'active' : ''}`}
          onClick={() => props.setActiveTab(tabName)}
        >
          {tabName}
        </span>
      ))}
    </header>
  );
};

export default Header;