import React from 'react';
import Counter from './Counter';
import Countries from './Countries';

const Content = (props) => {
  return (
    <div className="content-container">
      {props.activeTab === 'Counter'
        ? <Counter />
        : <Countries />}
    </div>
  );
};

export default Content;