import React, { useState } from 'react';
import Header from './features/Header';
import Content from './features/Content';

function App() {
  const [tab, setTab] = useState('Counter');

  return (
    <div className="container">
      <Header tab={tab} setActiveTab={setTab} />
      <Content activeTab={tab} />
    </div>
  );
}

export default App;
